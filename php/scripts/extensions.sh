#!/usr/bin/env bash

set -euo pipefail


if [[ $PHP_VERSION == "8.0" ]]; then
  export extensions=" \
    bcmath \
    bz2 \
    calendar \
    exif \
    gmp \
    intl \
    mysqli \
    opcache \
    pcntl \
    pdo_mysql \
    soap \
    xsl \
    zip
    "
else
  export extensions=" \
    bcmath \
    bz2 \
    calendar \
    exif \
    gmp \
    intl \
    mysqli \
    opcache \
    pcntl \
    pdo_mysql \
    soap \
    xsl \
    zip
    "
fi

export buildDeps=" \
    default-libmysqlclient-dev \
    libbz2-dev \
    libsasl2-dev \
    pkg-config \
    "

export runtimeDeps=" \
    imagemagick \
    libfreetype6-dev \
    libgmp-dev \
    libicu-dev \
    libjpeg-dev \
    libkrb5-dev \
    libmagickwand-dev \
    libmemcached-dev \
    libmemcachedutil2 \
    libpng-dev \
    libpq-dev \
    libssl-dev \
    libuv1-dev \
    libwebp-dev \
    libxml2-dev \
    libxslt1-dev \
    libzip-dev
    "
apt-get update \
  && apt-get install -yq $buildDeps \
  && apt-get install -yq $runtimeDeps \
  && rm -rf /var/lib/apt/lists/* \
  && docker-php-ext-install -j$(nproc) $extensions

if [[ $PHP_VERSION == "8.0" || $PHP_VERSION == "7.4" ]]; then
  docker-php-ext-configure gd --with-freetype --with-jpeg --with-webp \
    && docker-php-ext-install -j$(nproc) gd \
    && PHP_OPENSSL=yes docker-php-ext-configure imap --with-kerberos --with-imap-ssl \
    && docker-php-ext-install -j$(nproc) imap \
    && docker-php-source delete
else
  docker-php-ext-configure gd \
    && docker-php-ext-install -j$(nproc) gd \
    && docker-php-ext-configure imap --with-kerberos --with-imap-ssl \
    && docker-php-ext-install -j$(nproc) imap \
    && docker-php-source delete
fi


echo 'memory_limit=1024M' > /usr/local/etc/php/conf.d/zz-conf.ini

apt-get purge -yqq --auto-remove $buildDeps
