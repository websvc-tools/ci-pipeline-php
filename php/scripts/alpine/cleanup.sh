#!/usr/bin/env bash

set -euo pipefail

rm -rf /tmp/* \
        /usr/includes/* \
        /usr/share/man/* \
        /var/cache/apk/* \
        /var/tmp/*
