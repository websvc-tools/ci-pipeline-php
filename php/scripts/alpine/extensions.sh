#!/usr/bin/env bash

set -euo pipefail

apk --update --no-cache add \
  bzip2 \
  bzip2-dev \
  curl-dev \
  freetype-dev \
  gmp-dev \
  icu-dev \
  imagemagick \
  imagemagick-dev \
  imap-dev \
  krb5-dev \
  libbz2 \
  libedit-dev \
  libintl \
  libjpeg-turbo-dev \
  libltdl \
  libmemcached-dev \
  libpng-dev \
  libtool \
  libxml2-dev \
  libxslt-dev \
  pcre-dev

apk --update --no-cache add libzip-dev libsodium-dev

if [[ $PHP_VERSION == "8.0" ]]; then
  PHP_OPENSSL=yes docker-php-ext-configure imap --with-kerberos --with-imap-ssl
  docker-php-ext-install -j "$(nproc)" imap
  docker-php-ext-install -j "$(nproc)" exif pcntl bcmath bz2 calendar intl mysqli opcache pdo_mysql soap xsl zip gmp
  docker-php-source delete
else
  PHP_OPENSSL=yes docker-php-ext-configure imap --with-kerberos --with-imap-ssl
  docker-php-ext-install -j "$(nproc)" imap
  docker-php-ext-install -j "$(nproc)" exif pcntl bcmath bz2 calendar intl mysqli opcache pdo_mysql soap xsl zip gmp
  docker-php-source delete
fi

if [[ $PHP_VERSION == "8.0" || $PHP_VERSION == "7.4" ]]; then
  docker-php-ext-configure gd --with-freetype --with-jpeg
else
  docker-php-ext-configure gd
fi

docker-php-ext-install -j "$(nproc)" gd

echo "memory_limit=1G" > /usr/local/etc/php/conf.d/zz-conf.ini
