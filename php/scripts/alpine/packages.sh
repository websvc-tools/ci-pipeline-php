#!/usr/bin/env bash

set -euo pipefail

apk --update --no-cache add \
    git \
    grep \
    jq \
    make \
    openssl \
    patch \
    rsync \
    sudo \
    zip

# persistent / runtime deps
apk add --update --no-cache --virtual .persistent-deps \
    ca-certificates \
    tar \
    xz \
    curl
