# CI Pipeline PHP

Docker images with tools needed to build and test PHP applications

This is a light weight version of `edbizarro/gitlab-ci-pipeline-php` images

All images based on PHP official images and include composer v2 by default (versions 7.3 and 7.4 have composer v1 as well `composer1`)

Note that new image releases overwrite the already existing one, so a manual pull at the runner may be required.

It's found that alpine images fail to build via CI.

## PHP versions

- 7.3
  - includes composer v1 and v2
- 7.4
  - includes composer v1 and v2
- 8.0
- 8.1.4

## Docker flavours

- default (debian based)
- alpine

## Gitlab CI examples

How to use it in a Gitlab CI Job

```yaml
# phpcs example
quality:phpcs:
  stage: quality
  tags:
    - docker
  image: websvc-tools/ci-pipeline-php:7.4
  script:
    - composer1 install --prefer-dist --no-interaction --no-progress
    - ./vendor/bin/phpcs .
  allow_failure: false
  except:
    # Runs for every push, skipped for tags and merge requests
    - tags
    - merge_requests
```

```yaml
# phpcs example
quality:phpcs:
  stage: quality
  tags:
    - docker
  image: websvc-tools/ci-pipeline-php:7.1.4-alpine
  script:
    - composer install --prefer-dist --no-interaction --no-progress
    - ./vendor/bin/phpcs .
  allow_failure: false
  except:
    # Runs for every push, skipped for tags and merge requests
    - tags
    - merge_requests
```
